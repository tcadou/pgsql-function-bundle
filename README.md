Installation
============


Installation
============

Step 1: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```bash
$ composer require "SimplyMove/DoctrinePgsqlBundle:dev-master"
```

Step 2: Add the new Types and Functions to the Config
-------------------------

```
# config.yml
doctrine:
    dbal:
        types:
          jsonb: SimplyMove\DoctrinePgsqlBundle\Types\JsonbArrayType
        mapping_types:
          jsonb: jsonb
    orm:
        dql:
            string_functions:
                JSONB_AG:   SimplyMove\DoctrinePgsqlBundle\Functions\JsonbAtGreater
                JSONB_HGG:  SimplyMove\DoctrinePgsqlBundle\Functions\JsonbHashGreater
                JSONB_EX:   SimplyMove\DoctrinePgsqlBundle\Functions\JsonbExistence
                ILIKE:   SimplyMove\DoctrinePgsqlBundle\Functions\ILIKE
                SUBSTRING:   SimplyMove\DoctrinePgsqlBundle\Functions\Substring
                AT_TIME_ZONE:   SimplyMove\DoctrinePgsqlBundle\Functions\AtTimeZoneFunction
                COUNT_FILTER:   SimplyMove\DoctrinePgsqlBundle\Functions\CountFilterFunction
                TO_CHAR:   SimplyMove\DoctrinePgsqlBundle\Functions\DateFormat
                TO_DATE:   SimplyMove\DoctrinePgsqlBundle\Functions\StrToDate
                STRING_AGG:   SimplyMove\DoctrinePgsqlBundle\Functions\StringAgg
```